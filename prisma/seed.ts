import { hash } from "bcryptjs";

import {Source, PrismaClient} from "@prisma/client";

async function seed() {
  const prisma = new PrismaClient();

  async function truncateTables() {
    try {
      // Truncate the tables in the desired order
      await prisma.$queryRaw`
        TRUNCATE "Song", "Tag", "Playlist", "User" RESTART IDENTITY CASCADE
      `;
      console.log("Tables truncated successfully.");
    } catch (error) {
      console.error("Error truncating tables:", error);
    }
  }

  truncateTables();
  try {
    // Create tags
    const tags = [
      { label: "ads" },
      { label: "pop" },
      { label: "rock" },
      { label: "metal" },
    ];

    const createdTags: any[] = [];

    for (const tagData of tags) {
      const existingTag = await prisma.tag.findUnique({
        where: { label: tagData.label },
      });

      if (existingTag) {
        createdTags.push(existingTag);
      } else {
        const createdTag = await prisma.tag.create({ data: tagData });
        createdTags.push(createdTag);
      }
    }
    // Create users
    const password = await hash("Password1", 12)
    const users = [
      {
        name: "Alice",
        email: "alice@example.com",
        password,
        role: "admin",
      },
      {
        name: "Bob",
        email: "bob@example.com",
        password,
      },
    ];
    const createdUsers = await prisma.user.createMany({ data: users });


    // Create songs
    const songs = [
      {
        label: "Winamp llama ass",
        skipVotersCount: 0,
        tags: ["ads"],
        source: Source.Youtube,
        url: "https://www.youtube.com/watch?v=cKqKrH0O9yg",
        requesterEmail: "alice@example.com",
      },
      {
        label: "KYUSS - space cadet",
        skipVotersCount: 0,
        tags: ["metal"],
        source: Source.Youtube,
        url: "https://www.youtube.com/watch?v=dq8YcRi7DD4",
        requesterEmail: "alice@example.com",
      },
    ];

    const createdSongs = [];

    for (const songData of songs) {
      const tagsToConnect = songData.tags.map((tagLabel) =>
        (createdTags as any[]).find((tag) => tag.label === tagLabel)
      ).map(tag => ({id: tag.id}));
      console.log({tagsToConnect})

      const requester = await prisma.user.findUnique({
        where: { email: songData.requesterEmail },
      });

      if (requester) {
        const createdSong = await prisma.song.create({
          data: {
            label: songData.label,
            skipVotersCount: songData.skipVotersCount,
            tags: { connect: tagsToConnect },
            source: songData.source,
            url: songData.url,
            requester: { connect: { id: requester.id } },
          },
        });

        createdSongs.push(createdSong);
      }
    }
    // Create playlist
    const playlist = {
      label: "PL-123",
      songs: { connect: createdSongs.map((song: any) => ({ id: song.id })) },
      creator: { connect: { email: "alice@example.com" } },
    };
    const createdPlaylist = await prisma.playlist.create({ data: playlist });

    console.log("Seed data created successfully.");

    // Log the created data
    console.log("Tags:", createdTags);
    console.log("Users:", createdUsers);
    console.log("Songs:", createdSongs);
    console.log("Playlist:", createdPlaylist);
  } catch (error) {
    console.error("Error seeding data:", error);
  } finally {
    await prisma.$disconnect();
  }
}

seed();
