"use client";

import Footer from "../Partials/Footer";
import React from "react";
import { IoMdEyeOff, IoMdEye } from "react-icons/io";
import { useRouter, useSearchParams } from "next/navigation";
import { signIn } from "next-auth/react";
import { AiOutlineLoading3Quarters } from "react-icons/ai";
import Loading from "../Components/Loading";

export default function Login() {
  const router = useRouter();
  const searchParams = useSearchParams();
  const [passwordValue, setPasswordValue] = React.useState("");
  const [ofPassword, showPassword] = React.useState(true);
  const [loading, setLoading] = React.useState(false);
  const callbackUrl = searchParams.get("callbackUrl") || "/";
  const [error, setError] = React.useState("");

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    try {
      setLoading(true);
      const formData = new FormData(e.target);
      const email = formData.get("email");
      const password = formData.get("password");

      const res = await signIn("credentials", {
        redirect: true,
        email,
        password,
        callbackUrl,
      });

      if (!res?.error) {
        router.push(callbackUrl);
      } else {
        setError("invalid email or password");
      }
    } catch (error: any) {
      setLoading(false);
      setError(error);
    }
  };
  if (loading === true) {
    <Loading/>
  }

  return (
    <main
      className={`w-full h-screen bg-gradient-to-b  text-slate-700 from-indigo-300 via-blue-300 to-sky-300`}
    >
      <div className="w-[960px] mx-auto h-full flex items-center justify-center">
        <div className=" px-4 py-4 bg-opacity-70 bg-slate-50 rounded border-slate-700">
          <div className="flex-row items-center justify-center mb-4 text-center">
            <div className="text-6xl font-bold text-slate-800">PLM</div>
            <div className="italic text-slate-600">Playlist Manager</div>
          </div>
          <div className="grid grid-flow-row gap-4">
            <form
              onSubmit={handleSubmit}
              className="bg-slate-50 p-4 rounded grid-flow-row grid gap-4  w-[240px] mx-auto"
            >
              {error && (
                <p className="text-center bg-red-300 py-4 mb-6 rounded">
                  {error}
                </p>
              )}
              <div className="input-group w-full">
                <input
                  placeholder="E-mail"
                  type="text"
                  name="email"
                  className="border-slate-400 border text-sm w-full shadow outline-none bg-slate-200  p-1"
                />
              </div>
              <div className="input-group w-full relative">
                {ofPassword ? (
                  <input
                    placeholder="Mot de passe"
                    value={passwordValue}
                    name="password"
                    type="password"
                    onChange={(e) => setPasswordValue(e.target.value)}
                    className="border-slate-400 border text-sm w-full shadow outline-none bg-slate-200 p-1"
                  />
                ) : (
                  <input
                    placeholder="Mot de passe"
                    type="text"
                    name="password"
                    value={passwordValue}
                    onChange={(e) => setPasswordValue(e.target.value)}
                    className="border-slate-400 border text-sm w-full shadow outline-none bg-slate-200 p-1"
                  />
                )}
                <div className="absolute mr-1 right-0 top-0 bottom-0 text-xl cursor-pointer flex items-center">
                  {ofPassword === false ? (
                    <IoMdEyeOff
                      onClick={() => showPassword(true)}
                      className="hover:text-slate-400"
                    />
                  ) : (
                    <IoMdEye
                      onClick={() => showPassword(false)}
                      className="hover:text-slate-400"
                    />
                  )}
                </div>
              </div>
              <button className="bg-gradient-to-b from-blue-400 to-sky-500 hover:from-sky-800 hover:to-sky-400 text-slate-50 px-1 py-1 rounded">
                Se connecter
              </button>
            </form>
            <div className="bg-slate-50 p-4 rounded text-sm">
              Pas de compte?{" "}
              <a href="/register" className="underline">
                Inscrivez-vous
              </a>
            </div>
          </div>
        </div>
      </div>
      <Footer></Footer>
    </main>
  );
}
