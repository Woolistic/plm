import { Tag, TagStatus } from "@prisma/client";
import { create } from "zustand";
import { devtools } from "zustand/middleware";

interface TagState {
  tags: Tag[];
  addTag: ({
    id,
    label,
    tagStatus,
  }: {
    id: string;
    label: string;
    tagStatus: TagStatus;
  }) => void;
  removeTag: ({ id }: { id: string }) => void;
}

const useTagStore = create<TagState>()(
  devtools((set) => ({
    tags: [],
    addTag: ({ id, label, tagStatus }) =>
      set((state) => ({
        ...state,
        tags: [
          ...state.tags,
          {
            id,
            label,
            tagStatus,
          },
        ],
      })),
    removeTag: ({ id }) =>
      set((state) => ({
        ...state,
        tags: state.tags.filter((tag) => tag.id !== id),
      })),
  }))
);
export { useTagStore };
