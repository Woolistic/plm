import { create } from "zustand";
import { devtools } from "zustand/middleware";
import { Prisma, Song, Tag } from "@prisma/client";

export type PlaylistWithSong = Prisma.PlaylistGetPayload<{
  include: { songs: { include: { tags: true } } };
}>;
export type SongWithTag = Prisma.SongGetPayload<{
  include: { tags: true };
}>;

export interface PlaylistState {
  playlist: PlaylistWithSong | {};
  setSongs: (songs: Song[]) => void;
  setPlaylist: (playlist: PlaylistWithSong | {}) => void;
  spreadTagChangeOnPlaylist: (payload: Tag) => void;
  addSong: (song: any) => void;
}

const usePlaylistStore = create<PlaylistState>()(
  devtools((set) => ({
    playlist: {},
    setPlaylist: (playlist) => set((state) => ({ ...state, playlist })),
    addSong: (song: Song) =>
      set((state) => {
        let songs: any[] = [];
        if (
          state.playlist != null &&
          (state.playlist as PlaylistWithSong).songs != null
        ) {
          songs = (state.playlist as PlaylistWithSong).songs;
        }
        return {
          ...state,
          playlist: {
            ...state.playlist,
            songs: [...songs, song],
          },
        };
      }),
    setSongs: (songs: Song[]) =>
      set((state) => ({
        ...state,
        playlist: {
          ...state.playlist,
          songs,
        },
      })),
    spreadTagChangeOnPlaylist: (payload: Tag) =>
      set((state) => ({
        ...state,
        playlist: {
          ...state.playlist,
          songs: (state.playlist as PlaylistWithSong).songs.map(
            (song: SongWithTag) => ({
              ...song,
              tags:
                song.tags.length == 0
                  ? []
                  : song.tags.map((tag: Tag) =>
                      tag.id != payload.id
                        ? tag
                        : { ...tag, tagStatus: payload.tagStatus }
                    ),
            })
          ),
        },
      })),
  }))
);

export { usePlaylistStore };
