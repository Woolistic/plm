import { create } from "zustand";
import { devtools } from "zustand/middleware";

interface CursorState {
  cursor: number;
  nextSong: (songLength: number) => void;
  previousSong: (songLength: number) => void
}

const useCursorStore = create<CursorState>()(
  devtools((set) => ({
    cursor: 0,
    nextSong: (songLength) =>
      set((state) => ({
        cursor:  songLength - 1 === state.cursor ? 0: state.cursor + 1
      })),
    previousSong: (songLength) =>
      set((state) => ({
        cursor: state.cursor === 0 ? songLength - 1 : state.cursor - 1
      }))
  }))
);
export { useCursorStore };
