import { getServerSession } from "next-auth";
import { User } from "@prisma/client";
import React from "react";
import prisma from "@/lib/prisma";

import { authOptions } from "@/lib/auth";
import Header from "@/app/Partials/Header";
import ProfileMenu from "@/app/Partials/ProfileMenu";
import Footer from "@/app/Partials/Footer";
import Loading from "../Components/Loading";
import Playlist from "@/app/Partials/Playlist";
import { redirect } from "next/navigation";

export default async function Player() {
  const session = await getServerSession(authOptions);
  const user = session?.user;

  if (session == null || user == null) {
    redirect("/login");
  }

  const { playlist }: any = await prisma.user.findFirst({
    where: {
      id: (user as User).id,
    },
    include: {
      playlist: {
        include: {
          songs: {
            include: {
              requester: true,
              tags: true,
            },
          },
        },
      },
    },
  });
  if (playlist == null) {
    <Loading />;
  }
  return (
    <main className="w-full h-screen bg-slate-100 text-slate-700 dark:bg-slate-700 dark:text-slate-50">
      <Header playlist={playlist} />
      <div className="container mx-auto max-w-[960px]">
        <div className="grid grid-cols-8">
          <div className="col-span-2">
            <ProfileMenu user={user} playlist={playlist} />
          </div>
          <div className="col-span-6 border-red-200">
            <Playlist user={user} playlist={playlist} />
          </div>
        </div>
      </div>
      <Footer />
    </main>
  );
}
