import { NextResponse } from "next/server";
import prisma from "@/lib/prisma";
import _ from "lodash";

export async function GET(request: Request) {
  const { searchParams } = new URL(request.url);
  const id = searchParams.get("id");
  if (id) {
    const user = await prisma.user.findFirst({
      where: { id },
      include: {
        playlist: {
          include: {
            songs: true,
          },
        },
      },
    });
    const userNoPassword = _.omit(user, "password")
    return NextResponse.json({user: userNoPassword}, { status: 200 });
  }
  return NextResponse.json({}, { status: 200 });
}
