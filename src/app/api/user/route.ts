import { NextResponse } from "next/server";
import prisma from "@/lib/prisma";

export async function GET(request: Request) {
  const { searchParams } = new URL(request.url);
  const name = searchParams.get("name");
  const email = searchParams.get("email");
  if (name) {
    const user = await prisma.user.findFirst({
      where: { name },
    });
    return NextResponse.json(user, { status: 200 });
  }
  if (email) {
    const user = await prisma.user.findFirst({
      where: { email },
    });
    return NextResponse.json(user, { status: 200 });
  }
  return NextResponse.json({}, { status: 200 });
}
