"use client";
import { FaStarOfLife } from "react-icons/fa";
import { BsArrowLeftShort } from "react-icons/bs";
import Footer from "../Partials/Footer";
import React from "react";
import { IoMdEyeOff, IoMdEye } from "react-icons/io";
import { MdBlockFlipped, MdCheck } from "react-icons/md";
import { AiOutlineLoading3Quarters } from "react-icons/ai";
import { signIn } from "next-auth/react";

enum FETCH {
  LOADING,
  NOT_FOUND,
  FOUND,
}

export default function Register() {
  const [nameValue, setNameValue] = React.useState("");
  const [nameSchema, setNameSchema] = React.useState({
    minChars: false,
    noSpaces: false,
  });
  const [uniqueUser, setUniqueUser] = React.useState(FETCH.LOADING);
  const [emailValue, setEmailValue] = React.useState("");
  const [emailSchema, setEmailSchema] = React.useState({
    emailFormat: false,
  });
  const [uniqueEmail, setUniqueEmail] = React.useState(FETCH.LOADING);

  const [passwordValue, setPasswordValue] = React.useState("");
  const [ofPassword, showPassword] = React.useState(true);
  const [passwordSchema, setPasswordSchema] = React.useState({
    minChars: false,
    atLeastOneCapital: false,
    atLeastOneNumber: false,
  });
  let [loading, setLoading] = React.useState(false);
  const [errors, setErrors] = React.useState();

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const name = formData.get("username");
    const email = formData.get("email");
    const password = formData.get("password");
    try {
      const res = await fetch("/api/auth/register", {
        method: "POST",
        body: JSON.stringify({ name, email, password }),
        headers: {
          "Content-Type": "application/json",
        },
      });

      setLoading(false);
      if (!res.ok) {
        setErrors((await res.json()).message);
        return;
      }

      signIn(undefined, { callbackUrl: "/" });
    } catch (error: any) {
      setLoading(false);
      setErrors(error.message);
    }
  };

  const handleChangeName = (name: string) => {
    setUniqueUser(FETCH.LOADING);
    const isUniqueUser = async (name: string) => {
      if (name.length) {
        const data = await fetch(`/api/user?name=${name}`);
        const response = await data.json();
        return response == null ? FETCH.NOT_FOUND : FETCH.FOUND;
      }
      return FETCH.LOADING;
    };
    isUniqueUser(name).then((result) => setUniqueUser(result));
    setNameSchema((state) => ({
      ...state,
      minChars: name.length >= 2,
      noSpaces: /\s/g.test(name) === false,
    }));

    setNameValue(name);
  };
  const handleChangeEmail = (email: string) => {
    setUniqueEmail(FETCH.LOADING);
    const isUniqueEmail = async (email: string) => {
      if (email.length) {
        const data = await fetch(`/api/user?email=${email}`);
        const response = await data.json();
        return response == null ? FETCH.NOT_FOUND : FETCH.FOUND;
      }
      return FETCH.LOADING;
    };
    isUniqueEmail(email).then((result) => setUniqueEmail(result));
    setEmailSchema(() => ({
      emailFormat:
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
          email
        ),
    }));
    setEmailValue(email);
  };
  const handleChangePassword = (password: string) => {
    setPasswordSchema(() => ({
      minChars: password.length >= 8,
      atLeastOneCapital: /(.*[A-Z].*)/.test(password),
      atLeastOneNumber: /(.*\d.*)/.test(password),
    }));
    setPasswordValue(password);
  };
  const fetchIcons = [
    {
      label: FETCH.LOADING,
      icon: <AiOutlineLoading3Quarters className="animate-spin" />,
    },
    { label: FETCH.FOUND, icon: <MdBlockFlipped className="inline" /> },
    { label: FETCH.NOT_FOUND, icon: <MdCheck className="inline" /> },
  ];
  return (
    <main
      className={`w-full h-screen bg-gradient-to-b  text-slate-700 from-indigo-300 via-blue-300 to-sky-300`}
    >
      <div className="w-[960px] mx-auto h-full flex items-center justify-center">
        <div className=" px-4 py-4 bg-opacity-70 bg-slate-50 rounded border-slate-700">
          <div>
            <a href="/" className="underline inline-flex items-center gap-0">
              <BsArrowLeftShort />
              login
            </a>
          </div>
          <div className="flex-row items-center justify-center mb-4 text-center">
            <div className="text-6xl font-bold text-slate-800">PLM</div>
            <div className="italic text-slate-600">Playlist Manager</div>
          </div>

          <div className="grid grid-flow-row gap-4">
            <form
              onSubmit={handleSubmit}
              className="bg-slate-50 p-4 rounded grid-flow-row grid gap-4  w-[320px] mx-auto"
            >
              <div className="text-xs italic">
                Les champs marqués d’un astérisque 
                <sup>
                  <FaStarOfLife className="inline text-xs text-red-500" />
                </sup>
                sont obligatoires.
              </div>
              <div className="input-group w-full relative">
                <div className="font-semibold">Nom d’utilisateur</div>
                <input
                  placeholder="Nom d'utilisateur"
                  type="text"
                  name="username"
                  value={nameValue}
                  onChange={(e) => handleChangeName(e.target.value)}
                  className="border-slate-400 border text-sm w-full shadow outline-none bg-slate-200  p-1"
                />
                <div className="text-xs mr-1 mt-1 absolute top-0 right-0 bottom-0 text-red-500">
                  <FaStarOfLife />
                </div>
                <div className="text-xs italic">
                  <div className="flex items-center gap-1">
                    {nameSchema.minChars ? (
                      <MdCheck className="inline" />
                    ) : (
                      <MdBlockFlipped className="inline" />
                    )}
                    2 caractères minimum
                  </div>
                  <div className="flex items-center gap-1">
                    {fetchIcons.find((f) => uniqueUser === f.label)?.icon}
                    unique
                  </div>
                  <div className="flex items-center gap-1">
                    {nameSchema.noSpaces ? (
                      <MdCheck className="inline" />
                    ) : (
                      <MdBlockFlipped className="inline" />
                    )}
                    tout attaché
                  </div>
                </div>
              </div>
              <div className="input-group w-full relative">
                <div className="font-semibold">E-mail</div>
                <input
                  placeholder="nom@domaine.com"
                  type="email"
                  name="email"
                  value={emailValue}
                  onChange={(e) => handleChangeEmail(e.target.value)}
                  className="border-slate-400 border text-sm w-full shadow outline-none bg-slate-200  p-1"
                />
                <div className="text-xs mr-1 mt-1 absolute top-0 right-0 bottom-0 text-red-500">
                  <FaStarOfLife />
                </div>
                <div className="text-xs italic">
                  <div className="flex items-center gap-1">
                    {emailSchema.emailFormat ? (
                      <MdCheck className="inline" />
                    ) : (
                      <MdBlockFlipped className="inline" />
                    )}
                    format valide
                  </div>
                  <div className="flex items-center gap-1">
                    {fetchIcons.find((f) => uniqueEmail === f.label)?.icon}
                    unique
                  </div>
                </div>
              </div>
              <div className="input-group w-full relative">
                <div className="font-semibold">Mot de passe</div>
                <div className="text-xs mr-1 mt-1 absolute top-0 right-0 bottom-0 text-red-500">
                  <FaStarOfLife />
                </div>
                <div className="input-group w-full relative">
                  {ofPassword ? (
                    <input
                      placeholder="Mot de passe"
                      value={passwordValue}
                      name="password"
                      type="password"
                      onChange={(e) => handleChangePassword(e.target.value)}
                      className="border-slate-400 border text-sm w-full shadow outline-none bg-slate-200 p-1"
                    />
                  ) : (
                    <input
                      placeholder="Mot de passe"
                      type="text"
                      name="password"
                      value={passwordValue}
                      onChange={(e) => handleChangePassword(e.target.value)}
                      className="border-slate-400 border text-sm w-full shadow outline-none bg-slate-200 p-1"
                    />
                  )}
                  <div className="absolute mr-1 right-0 top-0 bottom-0 text-xl cursor-pointer flex items-center">
                    {ofPassword === false ? (
                      <IoMdEyeOff
                        onClick={() => showPassword(true)}
                        className="hover:text-slate-400"
                      />
                    ) : (
                      <IoMdEye
                        onClick={() => showPassword(false)}
                        className="hover:text-slate-400"
                      />
                    )}
                  </div>
                </div>
                <div className="text-xs italic">
                  <div className="flex items-center gap-1">
                    {passwordSchema.minChars ? (
                      <MdCheck className="inline" />
                    ) : (
                      <MdBlockFlipped className="inline" />
                    )}{" "}
                    8 caractères minimum
                  </div>
                  <div className="flex items-center gap-1">
                    {passwordSchema.atLeastOneCapital ? (
                      <MdCheck className="inline" />
                    ) : (
                      <MdBlockFlipped className="inline" />
                    )}
                    au moins une lettre majuscule
                  </div>
                  <div className="flex items-center gap-1">
                    {passwordSchema.atLeastOneNumber ? (
                      <MdCheck className="inline" />
                    ) : (
                      <MdBlockFlipped className="inline" />
                    )}
                    au moins un chiffre
                  </div>
                </div>
              </div>
              <button className="bg-gradient-to-b from-blue-400 to-sky-500 hover:from-sky-800 hover:to-sky-400 text-slate-50 px-1 py-1 rounded">
                S’enregistrer
              </button>
            </form>
          </div>
        </div>
      </div>
      <Footer></Footer>
    </main>
  );
}
