export { default } from "next-auth/middleware";

const config = {
  // matcher: ["/profile"],
  matcher: ["/((?!register|profile|api|login).*)"],
};

export {config}