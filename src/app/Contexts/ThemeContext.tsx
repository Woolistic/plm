"use client";

import React from "react";

export interface Theme {
  tagStatus: { allowed: string; forbidden: string };
}

export const ThemeContext = React.createContext({});

const Make = ({ children }: { children: React.ReactNode }) => {
  const [theme, setTheme] = React.useState({
    tagStatus: {
      allowed: "green",
      forbidden: "red",
      neutral: "gray"
    },
  });
  const defaultContext = { theme, setTheme };

  return (
    <ThemeContext.Provider value={defaultContext}>
      {children}
    </ThemeContext.Provider>
  );
};
export default Make;
