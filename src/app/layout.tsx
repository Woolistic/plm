import Theme from "./Contexts/ThemeContext";
import "./globals.css";
import { NextAuthProvider } from "./providers";

export const metadata = {
  title: "PLM",
  description: "Playlist Manager",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        <NextAuthProvider>
          <Theme>{children}</Theme>
        </NextAuthProvider>
      </body>
    </html>
  );
}
