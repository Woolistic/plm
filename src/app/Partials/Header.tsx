"use client"
import {
  AiFillStepBackward,
  AiOutlinePause,
  AiFillStepForward,
} from "react-icons/ai";
import { TbRectangleFilled } from "react-icons/tb";

import { IoPlaySharp, IoStopSharp } from "react-icons/io5";
import { useCursorStore } from "../Stores/CursorStore";

export default function Header({ playlist }: { playlist: any }): JSX.Element {
  const [previousSong, nextSong] = useCursorStore((state) => [
    state.previousSong,
    state.nextSong,
  ]);
  const songLength = playlist.songs.length;

  return (
    <div className="w-full bg-slate-50 text-slate-800">
      <div className="container w-[480px] mx-auto">
        <div className="grid grid-cols-12 gap-4 py-1 text-xl items-center">
          <div className="py-2 relative col-span-10">
            <div className="absolute top-0 bottom-0 left-[0px] text-slate-500 font-bold">
              <TbRectangleFilled />
            </div>
            <div className="w-full h-1 bg-slate-400 "></div>
          </div>
          <div className="col-span-2 text-xs">
            0:00/
            <span className="font-bold">0:09</span>
          </div>
        </div>
        <div className="grid grid-flow-col gap-4 py-1 text-xl">
          <button className="cursor-pointer hover:text-blue-200">
            <AiFillStepBackward onClick={() => previousSong(songLength)} />
          </button>
          <div className="cursor-pointer hover:text-blue-200">
            <IoPlaySharp />
          </div>
          <div className="cursor-pointer hover:text-blue-200">
            <AiOutlinePause />
          </div>
          <div className="text-red-500">
            <IoStopSharp />
          </div>
          <button className="cursor-pointer hover:text-blue-200">
            <AiFillStepForward onClick={() => nextSong(songLength)} />
          </button>
        </div>
      </div>
    </div>
  );
}
