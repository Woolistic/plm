/* eslint-disable react-hooks/exhaustive-deps */
"use client";
import FlippableBadge from "../Components/FlippableBadge";
import { ThemeContext } from "../Contexts/ThemeContext";
import React, { useContext, useEffect } from "react";
import { useTagStore } from "../Stores/TagStore";
import AddFlippableBadge from "../Components/AddFlippableBadge";
import {
  PlaylistWithSong,
  SongWithTag,
  usePlaylistStore,
} from "../Stores/PlaylistStore";
import PlaylistSongs from "../Components/PlaylistSongs";
import { Tag, TagStatus } from "@prisma/client";
import { MdFilterAlt } from "react-icons/md";
import AddSong from "../Components/AddSong";
import crypto from "crypto";
import { useCursorStore } from "../Stores/CursorStore";

export default function Playlist({
  user,
  playlist,
}: {
  user: any;
  playlist: PlaylistWithSong;
}) {
  const { theme }: any = useContext(ThemeContext);
  const setSongs = usePlaylistStore((state: any) => state.setSongs);
  const tags = useTagStore((state: any) => state.tags);
  const addTag = useTagStore((state: any) => state.addTag);
  const removeTag = useTagStore((state: any) => state.removeTag);
  const statePlaylist = usePlaylistStore((state: any) => state.playlist);
  const spreadTagChangeOnPlaylist = usePlaylistStore(
    (state: any) => state.spreadTagChangeOnPlaylist
  );
  const addSong = usePlaylistStore((state: any) => state.addSong);
  const cursor = useCursorStore((state) => state.cursor);
  useEffect(() => {
    setSongs(playlist.songs);
  }, []);

  function incrementSkipVoters(songId: string) {
    const updated = (statePlaylist as PlaylistWithSong).songs.map((song) => {
      if (song.id === songId) {
        return { ...song, skipVotersCount: song.skipVotersCount + 1 };
      }
      return song;
    });
    setSongs(updated);
  }

  function onFlip(tagId: string, newFlip: TagStatus) {
    const updated = (statePlaylist as PlaylistWithSong).songs.map(
      (song: SongWithTag) => {
        if (song.tags == null || song.tags.length == 0) {
          return song;
        }
        return {
          ...song,
          tags: song.tags.map((tag: Tag) => {
            console.log({ tag });
            if (tag.id === tagId) {
              const tagResult = {
                ...tag,
                tagStatus: newFlip,
              };
              spreadTagChangeOnPlaylist(tagResult);
              return tagResult;
            }
            return tag;
          }),
        };
      }
    );
    setSongs(updated);
  }

  function handleCloseBadge({ id }: { id: string }) {
    removeTag({ id });
    const updated = (statePlaylist as PlaylistWithSong).songs.map(
      (song: SongWithTag) => {
        if (song.tags == null || song.tags.length == 0) {
          return song;
        }
        return {
          ...song,
          tags: song.tags.map((tag: Tag) => {
            if (tag.id === id) {
              return {
                ...tag,
                tagStatus: TagStatus.Neutral,
              };
            }
            return tag;
          }),
        };
      }
    );
    setSongs(updated);
  }

  function handleAddTag(tag: Tag) {
    const foundTag =
      tags.find((stateTag: any) => stateTag.id === tag.id) != undefined;
    if (foundTag === false) {
      spreadTagChangeOnPlaylist(tag);
      addTag(tag);
    } else {
      console.warn(`tag «${tag.label}» already in the pool`);
    }
  }
  
  function handleAddSong(payload: any) {
    const label = payload.label;
    const skipVotersCount = 0;
    const tags = payload.tags || [];
    const source = payload.source;
    const url = payload.url;
    addSong({
      id: crypto.randomBytes(20).toString("hex"),
      label,
      skipVotersCount,
      source,
      url,
      tags,
      requester: { id: user.id },
    });
  }

  if (
    statePlaylist == null ||
    Object.keys(statePlaylist).length == 0 ||
    statePlaylist.songs == null
  ) {
    return <>Loading…</>;
  }

  return (
    <div className="container w-[480px] mt-8">
      <div className="w-full flex flex-wrap gap-2 items-center">
        <MdFilterAlt />
        {tags.length > 0 ? (
          tags.map(({ id, tagStatus, label }: any) => (
            <FlippableBadge
              key={`${id}/${label}`}
              id={id}
              theme={theme}
              tagStatus={tagStatus}
              onClose={handleCloseBadge}
              onFlip={onFlip}
            >
              <span>{label}</span>
            </FlippableBadge>
          ))
        ) : (
          <></>
        )}
        <AddFlippableBadge
          placeholder="Ajouter un filtre…"
          onSubmit={handleAddTag}
        />
      </div>
      <div className="w-full">
        <div className="w-full song-group ">
          <PlaylistSongs
            theme={theme}
            playlist={statePlaylist}
            cursor={cursor}
            incrementSkipVoters={incrementSkipVoters}
          />
        </div>
      </div>
      <AddSong onSubmit={handleAddSong} />
    </div>
  );
}
