import { AiOutlineGitlab } from "react-icons/ai"
import  pkgInfo from "../../../package.json"

export default function Footer(){
  return <div className="container bottom-0 absolute w-full text-slate-900">
    <div className="flex items-center justify-center gap-1 mx-auto w-[960px]">
      <div className="font-semibold">{pkgInfo.name}</div>
      <span>•</span>
      <div className="">version {pkgInfo.version}</div>
      <span>•</span>
      <a href={pkgInfo.homepage}
      className="bg-slate-50 p-1 text-slate-700 rounded-full hover:bg-slate-800 hover:text-slate-50"
      target="_blank" rel="noopener noreferrer">
        <AiOutlineGitlab/>
      </a>
    </div>
  </div>
}