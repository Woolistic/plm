"use client"
import { HiOutlineCog6Tooth } from "react-icons/hi2";
import { MdHeadphones, MdOutlineLogout } from "react-icons/md";
import { MdEdit } from "react-icons/md";
import { BsArrowRightShort } from "react-icons/bs";
import { AiOutlinePlus } from "react-icons/ai";
import Image from "next/image";
import { PlaylistWithSong } from "../Stores/PlaylistStore";
import { signOut } from "next-auth/react"

export default function ProfileMenu({
  playlist,
  user
}: {
  playlist: any;
  user: any
}) {
  return (
    <div className="mx-2 mt-3">
      <div className="border w-12 h-12 mx-auto">
        {user.photo ? 
          <Image
          src="https://placebeard.it/640x360"
          className="w-full h-full"
          width={3 * 16}
          height={3 * 16}
          alt="profile picture" />
        : <Image
          src="https://placebeard.it/640x360"
          className="w-full h-full"
          width={3 * 16}
          height={3 * 16}
          alt="profile picture"
        />}
        
      </div>
      <div className="mx-auto flex justify-center items-start">{user.name}</div>
      <div className="mt-2 gap-3 flex justify-center items-start">
        <div className="flex-row gap-0 cursor-pointer justify-center items-center text-pink-700">
          <MdHeadphones className="cursor-pointer mx-auto text-center" />
          <span className="uppercase  text-xs font-bold">live</span>
        </div>
        <HiOutlineCog6Tooth className="cursor-pointer" />
        <button className="hover:text-sky-600" onClick={() => signOut()}>
          <MdOutlineLogout className="cursor-pointer" />
        </button>
      </div>
      <hr className="w-1/2 mx-auto my-1" />
      <div className="mt-2 gap-3 flex justify-center items-center">
        <div className="flex justify-center text-lg items-center">
          <BsArrowRightShort />
        </div>
        <div className=" cursor-pointer">
          {playlist?.label ?? ""}
        </div>
        <div className=" cursor-pointer">
          <MdEdit />
        </div>
      </div>
      <div className="mt-2 gap-3 flex justify-center items-center">
        <div className="bg-slate-50 inline-flex items-center px-1 border rounded text-sm w-32">
          <input
            type="text"
            name="add_music_playlist"
            className="w-full outline-none"
            placeholder="Ajouter une playlist…"
          />
          <AiOutlinePlus className="cursor-pointer" />
        </div>
      </div>
    </div>
  );
}
