import { Source } from "@prisma/client";
import { FaDeezer } from "react-icons/fa";
export default function SongSearchDeezer({
  inputValue,
  onSelect,
}: {
  inputValue: string;
  onSelect: (...values: any) => any;
}) {
  const source= Source.Deezer
  const url = "https://google.com"
  const label = "SongSearchDeezer"

  return (
    <>
      <div className="grid-flow-row gap-3 divide-y-1">
        <div className="flex gap-1 flex-wrap items-center hover:bg-gray-200 cursor-pointer"
        onClick={() => onSelect({ source, url, label })}>
          <FaDeezer />
          SongSearchDeezer
        </div>
      </div>
    </>
  );
}
