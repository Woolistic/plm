import type { Tag } from "@prisma/client";

export default function TagResults({
  results,
  onSelect,
}: {
  results: Tag[];
  onSelect: (...values: any) => any;
}) {
  return (
    <div>
      {results.map((result: any) => {
        const { item }: { item: Tag } = result;
        return (
          <div
            className="hover:bg-slate-300 cursor-pointer px-1"
            key={item.id}
            onClick={() => onSelect({...item})}
          >
            {item.label}
          </div>
        );
      })}
    </div>
  );
}
