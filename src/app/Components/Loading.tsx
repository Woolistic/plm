import { AiOutlineLoading3Quarters } from "react-icons/ai";

const Loading = () => (
  <main
    className={`w-full h-screen bg-gradient-to-b  text-slate-700 from-indigo-300 via-blue-300 to-sky-300`}
  >
    <div className="w-[960px] mx-auto h-full flex items-center justify-center">
      <AiOutlineLoading3Quarters className="animate-spin text-3xl" />
    </div>
  </main>
);
export default Loading