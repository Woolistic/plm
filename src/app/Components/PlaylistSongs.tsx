"use client"
import { Source, Tag } from "@prisma/client";
import {
  PlaylistState,
  PlaylistWithSong,
  SongWithTag,
  usePlaylistStore,
} from "../Stores/PlaylistStore";
import { AiFillYoutube, AiFillStepForward } from "react-icons/ai";
import SongTagBadge from "./SongTagBadge";
import {
  FaBandcamp,
  FaDailymotion,
  FaDeezer,
  FaSoundcloud,
  FaSpotify,
} from "react-icons/fa";
import { BsArrowRightShort } from "react-icons/bs";
import AddFlippableBadge from "./AddFlippableBadge";
import React from "react";

const SongSourceMap = [
  { label: Source.Youtube, icon: <AiFillYoutube /> },
  { label: Source.Deezer, icon: <FaDeezer /> },
  { label: Source.Spotify, icon: <FaSpotify /> },
  { label: Source.Dailymotion, icon: <FaDailymotion /> },
  { label: Source.Bandcamp, icon: <FaBandcamp /> },
  { label: Source.SoundCloud, icon: <FaSoundcloud /> },
];

export default function PlaylistSongs({
  playlist,
  theme,
  incrementSkipVoters,
  cursor = 0,
}: {
  playlist: PlaylistWithSong;
  theme?: any;
  incrementSkipVoters: (...values: any) => any;
  cursor: number;
}) {
  const setSongs = usePlaylistStore((state: PlaylistState) => state.setSongs);
  function addTagToSong(songId: string) {
    return function (tag: Tag) {
      if (Object.keys(playlist).length > 0) {
        const data = (playlist as PlaylistWithSong).songs.map(
          (song: SongWithTag) => {
            if (song.id === songId) {
              const foundTag =
                song.tags.find((songTag: Tag) => songTag.id === tag.id) !=
                undefined;
              if (foundTag) {
                return song;
              }
              return { ...song, tags: [...song.tags, tag] };
            }
            return song;
          }
        );
        setSongs(data);
      }
    };
  }

  return (
    <>
      {(playlist as PlaylistWithSong).songs.map((song: any, index: number) => {
        const songSource = SongSourceMap.find(
          (songSource) => songSource.label === song.source
        );
        return (
          <div className="grid grid-cols-12" key={song.id}>
            <div className="flex justify-center text-lg items-center">
              {index === cursor ? <BsArrowRightShort /> : <>•</>}
            </div>
            <div className="col-span-11 flex flex-wrap gap-1 items-center">
              <div className="text-lg" key={song.id}>
                {songSource != undefined
                  ? songSource.icon
                  : song.source?.slice(0, 16)}
              </div>
              <div className="font-semibold">
                <a className="hover:underline" href={song.url}>
                  {song.label}
                </a>
              </div>
              {song.requester == null ? <></>:<div className="italic">
                <a
                  href={`https://twitch.tv/${song.requester.name}`}
                  className="hover:underline"
                >
                  {song.requester.name}
                </a>
              </div>}
              <div className="inline-flex gap-0 items-center">
                <div
                  className="text-xs hover:bg-slate-700 p-1 inline-flex items-center cursor-pointer h-full border-b text-slate-100 border-b-slate-900 bg-slate-400"
                  onClick={() => incrementSkipVoters(song.id, playlist)}
                >
                  Skip <AiFillStepForward />
                </div>
                <div className="bg-slate-200 px-1">{song.skipVotersCount}</div>
              </div>
            </div>
            <div className="col-span-11 col-start-2 flex items-center gap-1 flex-wrap">
              {song.tags?.length ? (
                song.tags.map((tag: Tag) => (
                  <div className="col-span-11 col-start-2" key={tag.id}>
                    <SongTagBadge tag={tag} theme={theme} />
                  </div>
                ))
              ) : (
                <></>
              )}
              <AddFlippableBadge
                onSubmit={addTagToSong(song.id)}
                placeholder="Ajouter un genre…"
              />
            </div>
          </div>
        );
      })}
    </>
  );
}
