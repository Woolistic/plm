import styled from "@emotion/styled";
import { Tag } from "@prisma/client";
import React from "react";
import tw from "twin.macro";
import { TagStatus } from "@prisma/client";

const Badge = styled.div`
  ${tw`
      cursor-pointer
      inline-flex 
      items-center 
      px-1 
      border 
      rounded 
      text-sm  
      text-sky-100
  `}
`;

export default function SongTagBadge({ tag, theme }: { tag: Tag; theme: any }) {
  function setFlipTheme(tagStatus: TagStatus) {
    switch (tagStatus) {
      case TagStatus.Neutral :
        return theme.tagStatus.neutral;
      case TagStatus.Allowed:
        return theme.tagStatus.allowed;
      case TagStatus.Forbidden:
        return theme.tagStatus.forbidden
    }
  }
  const containerClassName = `bg-${setFlipTheme(tag.tagStatus)}-600`;
  return <Badge className={containerClassName}>{tag.label}</Badge>;
}
