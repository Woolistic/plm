import Fuse from "fuse.js";
import React from "react";
import type { Tag } from "@prisma/client";
import TagResults from "./TagResults";

export default function TagSearch({
  tags,
  inputValue,
  onSelect
}: {
  tags: Tag[];
  inputValue: string;
  onSelect:(...values: any) => any
}) {
  const fuse = new Fuse(tags, {
    keys: ["label", "tagStatus"],
  });
  const searchResults  = fuse.search(inputValue.trim()) as unknown as Tag[]; 
  if (searchResults.length) {
    return (
      <TagResults
        results={searchResults}
        onSelect={onSelect}
      />
    );
  }
  return <></>;
}
