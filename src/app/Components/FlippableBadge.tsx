import { MdBlockFlipped, MdCheck } from "react-icons/md";
import tw from "twin.macro";
import styled from "@emotion/styled";
import React from "react";
import { RxCross2 } from "react-icons/rx";
import { TagStatus } from "@prisma/client";

const Badge = styled.div`
  ${tw`
    inline-flex
    items-center
    px-1
    border
    rounded
    text-sm
    text-sky-100
  `}
`;

export default function FlippableBadge({
  id,
  children,
  tagStatus = TagStatus.Neutral,
  onFlip,
  theme,
  closable = true,
  onClose,
}: {
  id?: string;
  children: React.ReactNode;
  theme: any;
  tagStatus?: TagStatus;
  onFlip?: (...values: any) => any;
  closable?: boolean;
  onClose?: (...values: any) => any;
}) {
  function setFlipTheme(tagStatus?: TagStatus) {
    switch (tagStatus) {
      case TagStatus.Neutral :
        return theme.tagStatus.neutral;
      case TagStatus.Allowed:
        return theme.tagStatus.Allowed;
      case TagStatus.Forbidden:
        return theme.tagStatus.Forbidden
    }
  }
  function setFlipFromColor(color:string) {
    if (color === theme.tagStatus.neutral) {
      return TagStatus.Neutral
    }
    if (color === theme.tagStatus.forbidden) {
      return TagStatus.Forbidden
    }
    return TagStatus.Allowed
  }
  const [visible, toggleVisible] = React.useState(true);
  const [flip, setFlip] = React.useState(setFlipTheme(tagStatus));
  const containerClassName = `bg-${flip}-600`;
  const itemHoverClassName = `hover:bg-${flip}-800`;
  function handleFlip(newFlip: string) {
    if (onFlip != null) onFlip(id, setFlipFromColor(newFlip));
    setFlip(() => newFlip);
  }
  function handleClose(e: React.MouseEvent<HTMLElement>) {
    if (onClose != null) onClose({ id });
    if (closable == true) toggleVisible((flip) => !flip);
  }
  if (!visible) {
    return <></>;
  }
  return (
    <Badge className={containerClassName} id={id ?? ""}>
      <span className="px-1">{children}</span>
      <div className={`px-1 flex items-center  ${itemHoverClassName}`}>
        {flip === theme.tagStatus.allowed && (
          <div
            className={`cursor-pointer py-1 ${itemHoverClassName}`}
            onClick={() => handleFlip(theme.tagStatus.forbidden)}
          >
            <MdBlockFlipped />
          </div>
        )}
        {flip === theme.tagStatus.forbidden && (
          <div
            className={`cursor-pointer py-1 ${itemHoverClassName}`}
            onClick={() => handleFlip(theme.tagStatus.allowed)}
          >
            <MdCheck />
          </div>
        )}
        {flip === theme.tagStatus.neutral && (
          <div className="inline-flex items-center gap-1">
            <div
              className={`cursor-pointer py-1 ${itemHoverClassName}`}
              onClick={() => handleFlip(theme.tagStatus.forbidden)}
            >
              <MdBlockFlipped />
            </div>
            <div
              className={`cursor-pointer py-1 ${itemHoverClassName}`}
              onClick={() => handleFlip(theme.tagStatus.allowed)}
            >
              <MdCheck />
            </div>
          </div>
        )}
      </div>
      {closable && (
        <div
          className={`px-1 py-1 cursor-pointer ${itemHoverClassName}`}
          onClick={(e) => handleClose(e)}
        >
          <RxCross2 />
        </div>
      )}
    </Badge>
  );
}
