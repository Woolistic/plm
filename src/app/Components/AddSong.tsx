import React from "react";
import { AiOutlineEnter } from "react-icons/ai";
import SongSearch from "./SongSearch";

export default function AddSong({
  onSubmit,
}: {
  onSubmit: (...values: any) => any;
}) {
  const [inputValue, setInputValue] = React.useState("");
  function handleChange(e: any) {
    setInputValue(e.target.value);
  }
  function handleSubmit(values: any) {
    setInputValue("");
    onSubmit(values);
  }

  return (
    <div className=" col-span-12 w-full input-group mt-2 border bg-slate-50 rounded-sm">
      <div className="flex w-full gap-1">
        <input
          type="text"
          name="add_song_to_playlist"
          value={inputValue}
          onChange={(event: any) => handleChange(event)}
          onKeyDown={(e: any) =>
            e.key == "Enter" ? handleSubmit(inputValue) : null
          }
          placeholder="Ajouter une musique…"
          className="px-2 py-1 w-full bg-slate-50 mx-1 outline-none hover:border-bottom"
        />
        <div className="text-xl text-shadow p-1">
          <AiOutlineEnter
            onClick={() => handleSubmit(inputValue)}
            className="cursor-pointer"
          />
        </div>
      </div>
      {inputValue.length >= 2 && (
        <div className="w-full bg-slate-50 px-1 text-sm min-h-12 ">
          <SongSearch
            inputValue={inputValue}
            onSelect={handleSubmit}
          />
        </div>
      )}
    </div>
  );
}
