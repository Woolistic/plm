import React from "react";
import TagSearch from "./TagSearch";
import { Tag } from "@prisma/client";

export default function AddFlippableBadge({
  onSubmit,
  placeholder="Ajouter un tag…"
}: {
  onSubmit: (...values: any) => any;
  placeholder?:string
}) {
  const [tags, setTags] = React.useState([]);
  const [inputValue, setInput] = React.useState("");

  React.useEffect(() => {
    async function fetchData() {
      const data = await fetch("/api/tags");
      const response = await data.json();
      setTags(response);
    }
    fetchData();
  }, []);
  
  function handleChange(e: any) {
    setInput(e.target.value);
  }
  function handleSubmit(payload: Tag) {
    setInput("");
    onSubmit(payload);
  }

  if (tags.length) {
    return (
      <div className="relative">
        <div className="py-1 bg-slate-50 inline-flex items-center px-1 border rounded text-sm w-32">
          <input
            type="text"
            name="add_music_tag"
            value={inputValue}
            onChange={(event: any) => handleChange(event)}
            className="w-full outline-none"
            placeholder={placeholder}
          />
        </div>

        {inputValue.length > 1 && (
          <div className="absolute top-auto bg-slate-50 w-full px-1 text-sm min-h-12 ">
            <TagSearch
              tags={tags}
              inputValue={inputValue}
              onSelect={handleSubmit}
            />
          </div>
        )}
      </div>
    );
  }
  return <>Loading…</>;
}
