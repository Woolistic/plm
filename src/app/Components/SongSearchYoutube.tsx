import { Source } from "@prisma/client";
import { useEffect, useState } from "react";
import { AiFillYoutube } from "react-icons/ai";
import searchYoutube from "youtube-api-v3-search";

export default function SongSearchYoutube({
  inputValue,
  onSelect,
}: {
  inputValue: string;
  onSelect: (...values: any) => any;
}) {
  const [youtubeResults, setYoutubeResults] = useState([]);

  useEffect(() => {
    const options = {
      q: inputValue,
      part: "id,snippet",
      type: "video",
    };
    const YOUTUBE_KEY = process.env.NEXT_PUBLIC_YOUTUBE_API_KEY;
    async function fetchYoutube() {
      try {
        let result = await searchYoutube(YOUTUBE_KEY, options);
        setYoutubeResults(result.items);
      } catch (e) {
        console.error(e);
        setYoutubeResults([]);
      }
    }
    fetchYoutube();
  }, [inputValue, setYoutubeResults]);

  if (youtubeResults.length == 0) {
    return <></>;
  }
  return (
    <div>
      <div className="grid-flow-row gap-3 divide-y-1">
        {youtubeResults.map((video: any) => {
          const source = Source.Youtube;
          const url = `https://youtube.com/watch?v=${video.id.videoId}`;
          const label = video.snippet.title;

          return (
            <div
              onClick={() => onSelect({ ...video, source, url, label })}
              key={video.id.videoId}
              className="flex gap-1 items-center"
            >
              <span>
                <AiFillYoutube />
              </span>
              <span>{video.snippet.title.slice(0, 60)}…</span>
              <a
                href={`https://youtube.com/watch?v=${video.id.videoId}`}
                className="underline"
              >
                video
              </a>
            </div>
          );
        })}
      </div>
    </div>
  );
}
