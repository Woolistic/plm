import SongSearchDeezer from "./SongSearchDeezer";

export default function SongSearch({
  inputValue,
  onSelect,
  services = ["youtube", "deezer", "spotify", "dailymotion"],
}: {
  inputValue: string;
  onSelect: (...values: any) => any;
  services?: string[];
}) {
  return (
    <>
      <SongSearchDeezer inputValue={inputValue} onSelect={onSelect} />
    </>
  );
}
