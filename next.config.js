/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "placebeard.it",
        port: "",
        pathname: "/640x360",
      },
    ],
  }
};

module.exports = nextConfig;
